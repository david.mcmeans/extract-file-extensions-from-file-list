const regex = /\.([0-9a-z]+)[,| ]|([0-9a-z]+)$/gm;

// Alternative syntax using RegExp constructor
// const regex = new RegExp('\\.([0-9a-z]+)[,|"]', 'gm')

var s1 = jQuery('.view-content').text().trim();
//var s1 = jQuery('.view-content .views-field:eq(4)').text().trim();
console.log(s1);
let m, list = [];


while ((m = regex.exec(s1)) !== null) {
    // This is necessary to avoid infinite loops with zero-width matches
    if (m.index === regex.lastIndex) {
        regex.lastIndex++;
    }
    
    // The result can be accessed through the `m`-variable.
    m.forEach((match, groupIndex) => {
        console.log(`${groupIndex}: ${match}`);
        if (groupIndex >= 1) {
            if (match && !list.includes(match)) { list.push(match); }
        }
    });
}
console.log(list.join());