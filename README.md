# Extract file extensions from file list

Given a list of files in a Drupal website for a given content type such as:

```
https://cop.net/system/files/projects/2417/Report-05_31_2022.pdf, https://cop.net/system/files/projects/2417/Presentation-2022-04-27.pptx, https://cop.net/system/files/projects/2417/Recording%20-2022-04-27.mp4, https://cop.net/system/files/projects/2417/History-2021-2022.docx
https://cop.net/system/files/projects/2417/Recording-2022-10-04.mp4
```

use the extract.js in Developer Tools to pull out the list of file extensions.

This is useful for setting the allowed file extensions for a file field according to the files in use.

The Drupal 7 module, Allow all file extensions, must be removed to upgrade to Drupal 8/9/10. This module allows for a longer file extensions list than core permits. If you have a longer list than is permitted, you will want to reduce to what is actually in use before removing the module.